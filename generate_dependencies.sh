#!/bin/bash

cd ./wheels
num=10
for file in *; do
  if [ -f "$file" ]; then
    md5=$(md5sum $file | awk '{ print $1 }')
    echo "- name: PIP_INSTALL_${num}" >> ../dependencies.yml
    echo "  value: \"http://{{ .DependencyHost }}/packages/$file#md5=$md5\"" >> ../dependencies.yml
    num=$((num+1))
  fi
done
cd ..